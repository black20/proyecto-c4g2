package co.com.cesardiaz.misiontic.ventasdomiciliog2.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import co.com.cesardiaz.misiontic.ventasdomiciliog2.mvp.PaymentsMVP;

public class PaymentsInteractor implements PaymentsMVP.Model {

    private List<PaymentsMVP.PaymentDto> payments;

    public PaymentsInteractor(){
        payments = Arrays.asList(
                new PaymentsMVP.PaymentDto("Cesar Diaz", "Calle 1 # 7 - 53"),
                new PaymentsMVP.PaymentDto("Freddie Valenzuela", "Calle 2 # 15 - 23"),
                new PaymentsMVP.PaymentDto("Orlando Sedano", "Calle 3 # 2 - 55"),
                new PaymentsMVP.PaymentDto("Alexander Carrillo", "Cra 21 # 3 - 33"),
                new PaymentsMVP.PaymentDto("Cristian Loaiza", "Cra 22 # 7 - 56"),
                new PaymentsMVP.PaymentDto("Elizabeth Borja", "Urb. El Corral Mz A CS 1"),
                new PaymentsMVP.PaymentDto("Alexander Carrillo", "Cra 21 # 3 - 33"),
                new PaymentsMVP.PaymentDto("Cristian Loaiza", "Cra 22 # 7 - 56"),
                new PaymentsMVP.PaymentDto("Elizabeth Borja", "Urb. El Corral Mz A CS 1"),
                new PaymentsMVP.PaymentDto("Alexander Carrillo", "Cra 21 # 3 - 33"),
                new PaymentsMVP.PaymentDto("Cristian Loaiza", "Cra 22 # 7 - 56"),
                new PaymentsMVP.PaymentDto("Elizabeth Borja", "Urb. El Corral Mz A CS 1"),
                new PaymentsMVP.PaymentDto("Alexander Carrillo", "Cra 21 # 3 - 33"),
                new PaymentsMVP.PaymentDto("Cristian Loaiza", "Cra 22 # 7 - 56"),
                new PaymentsMVP.PaymentDto("Elizabeth Borja", "Urb. El Corral Mz A CS 1"),
                new PaymentsMVP.PaymentDto("Alexander Carrillo", "Cra 21 # 3 - 33"),
                new PaymentsMVP.PaymentDto("Cristian Loaiza", "Cra 22 # 7 - 56"),
                new PaymentsMVP.PaymentDto("Elizabeth Borja", "Urb. El Corral Mz A CS 1"),
                new PaymentsMVP.PaymentDto("Frank Polania", "Urb. El Corral Mz A CS 2")
        );

    }

    @Override
    public void loadPayments(LoadPaymentsCallback callback) {
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        callback.setPayments(payments);
    }
}
