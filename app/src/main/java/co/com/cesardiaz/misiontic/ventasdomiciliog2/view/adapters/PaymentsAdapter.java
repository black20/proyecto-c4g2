package co.com.cesardiaz.misiontic.ventasdomiciliog2.view.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import co.com.cesardiaz.misiontic.ventasdomiciliog2.R;
import co.com.cesardiaz.misiontic.ventasdomiciliog2.mvp.PaymentsMVP;

public class PaymentsAdapter extends RecyclerView.Adapter<PaymentsAdapter.ViewHolder> {

    private List<PaymentsMVP.PaymentDto> data;
    private OnItemClickListener listener;

    public PaymentsAdapter() {
        this.data = new ArrayList<>();
    }

    public void setData(List<PaymentsMVP.PaymentDto> data) {
        this.data = data;
        //notifyDataSetChanged();
    }

    public void setListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_payments, parent, false);

        return new ViewHolder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        PaymentsMVP.PaymentDto item = data.get(position);

        if (listener != null) {
            holder.itemView.setOnClickListener(v -> {
                listener.onItemClicked(item);
            });
        }

        holder.getTvName().setText(item.getClient());
        holder.getTvAddress().setText(item.getAddress());
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView ivClient;
        private TextView tvName;
        private TextView tvAddress;

        public ViewHolder(View view) {
            super(view);
            // Define click listener for the ViewHolder's View

            initUI(view);
        }

        private void initUI(View view) {
            ivClient = view.findViewById(R.id.iv_client);
            tvName = view.findViewById(R.id.tv_name);
            tvAddress = view.findViewById(R.id.tv_address);
        }

        public ImageView getIvClient() {
            return ivClient;
        }

        public TextView getTvName() {
            return tvName;
        }

        public TextView getTvAddress() {
            return tvAddress;
        }
    }

    public static interface OnItemClickListener {
        void onItemClicked(PaymentsMVP.PaymentDto item);
    }
}
