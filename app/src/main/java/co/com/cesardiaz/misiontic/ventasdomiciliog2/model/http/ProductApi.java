package co.com.cesardiaz.misiontic.ventasdomiciliog2.model.http;

import java.util.List;

import co.com.cesardiaz.misiontic.ventasdomiciliog2.model.http.dto.ProductResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface ProductApi {

    @GET("products")
    Call<List<ProductResponse>> getAll();

    @GET("products/{code}")
    Call<ProductResponse> getByCode(@Path("code") String code);
}
